package com.rewards.booksandrewards.repository;

import com.rewards.booksandrewards.model.Book;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BookRepository extends JpaRepository<Book, Long> {
}
