package com.rewards.booksandrewards.repository;

import com.rewards.booksandrewards.model.Transaction;
import com.rewards.booksandrewards.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface TransactionRepository extends JpaRepository<Transaction, Long> {

    List<Transaction> findByUserId(Long userId);

}
