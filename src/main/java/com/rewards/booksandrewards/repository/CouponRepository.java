package com.rewards.booksandrewards.repository;

import com.rewards.booksandrewards.model.Coupon;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CouponRepository extends JpaRepository<Coupon, Long> {
}
