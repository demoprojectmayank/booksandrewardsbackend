package com.rewards.booksandrewards.controller;

import com.rewards.booksandrewards.model.User;
import com.rewards.booksandrewards.service.AuthenticationService;
import com.rewards.booksandrewards.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/authentication")
public class AuthenticationController {

    @Autowired
    private AuthenticationService authenticationService;

    @Autowired
    private UserService userService;

    @PostMapping("sign-up")
    public ResponseEntity<?> signUp(@RequestBody User user) {
        if (userService.findByUsername(user.getUsername()).isPresent()) {
            return new ResponseEntity<>(new Exception("Username Or Email already exists in database"), HttpStatus.CONFLICT);
        }
        return new ResponseEntity<>(userService.createNewUser(user), HttpStatus.CREATED);
    }

    @PostMapping("sign-in")
    public ResponseEntity<?> signIn(@RequestBody User user) {
        try {
            return new ResponseEntity<>(authenticationService.signInAndReturnJwt(user), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(new Exception("Username or Password is wrong"), HttpStatus.BAD_REQUEST);
        }
    }
}
