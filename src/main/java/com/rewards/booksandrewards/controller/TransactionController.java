package com.rewards.booksandrewards.controller;

import com.rewards.booksandrewards.model.Transaction;
import com.rewards.booksandrewards.service.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("api")
public class TransactionController {

    @Autowired
    private TransactionService transactionService;

    @GetMapping("admin/transactions")
    public List<Transaction> getAllTransactions() {
        return transactionService.getAllTransactions();
    }

    @GetMapping("user/transactions/{username}")
    public List<Transaction> getTransactionsByUsername(@PathVariable String username) {
        return transactionService.getTransactionByUsername(username);
    }
}
