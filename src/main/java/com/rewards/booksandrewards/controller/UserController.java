package com.rewards.booksandrewards.controller;

import com.rewards.booksandrewards.model.enums.Role;
import com.rewards.booksandrewards.security.UserPrincipal;
import com.rewards.booksandrewards.service.CouponService;
import com.rewards.booksandrewards.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/users")
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private CouponService couponService;

    @GetMapping
    public ResponseEntity<?> getAllUsers() {
        return ResponseEntity.ok(userService.getAllUser());
    }

    @PutMapping("change/{role}")
    public ResponseEntity<?> changeRole(@AuthenticationPrincipal UserPrincipal userPrincipal, @PathVariable Role role) {
        userService.changeRole(role, userPrincipal.getUsername());
        return ResponseEntity.ok(true);
    }

    @PutMapping("switch-role/{username}")
    public ResponseEntity<?> changeRole(@PathVariable String username) {
        return ResponseEntity.ok(userService.switchRole(username));
    }

    @PutMapping("rewards/{couponId}/{username}")
    public ResponseEntity<?> rewardTicket(@PathVariable Long couponId, @PathVariable String username,
                                          @AuthenticationPrincipal UserPrincipal userPrincipal) {
        boolean response = userService.rewardTicketToUser(couponId, userPrincipal.getUsername(), username);
        if(response)return ResponseEntity.ok(couponService.getCouponsByUsername(userPrincipal.getUsername()));
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }
}
