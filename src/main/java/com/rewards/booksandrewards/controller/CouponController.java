package com.rewards.booksandrewards.controller;

import com.rewards.booksandrewards.model.Coupon;
import com.rewards.booksandrewards.service.CouponService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api")
public class CouponController {

    @Autowired
    private CouponService couponService;

    @GetMapping("admin/coupons")
    public ResponseEntity<?> getAllCoupons() {
        return ResponseEntity.ok(couponService.getAllCoupons());
    }

    @PostMapping("admin/coupons/add/{username}")
    public ResponseEntity<?> addCoupon(@RequestBody Coupon coupon, @PathVariable String username) {
        if (!couponService.addCoupon(coupon, username)) return new ResponseEntity<>(HttpStatus.NOT_ACCEPTABLE);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @GetMapping("user/coupons/{username}")
    public ResponseEntity<?> getCouponsByUsername(@PathVariable String username){
        return ResponseEntity.ok(couponService.getCouponsByUsername(username));
    }
}
