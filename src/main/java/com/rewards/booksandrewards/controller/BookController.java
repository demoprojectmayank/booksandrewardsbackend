package com.rewards.booksandrewards.controller;

import com.rewards.booksandrewards.model.Book;
import com.rewards.booksandrewards.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api")
public class BookController {

    @Autowired
    private BookService bookService;

    @GetMapping("admin/books")
    public ResponseEntity<?> getAllBooks(){
        return ResponseEntity.ok(bookService.getAllBooks());
    }

    @PostMapping("admin/books")
    public ResponseEntity<?> saveBook(@RequestBody Book book){
        bookService.saveBook(book);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @GetMapping("user/books/{username}")
    public ResponseEntity<?> getBooksByUsername(@PathVariable String username){
       return ResponseEntity.ok(bookService.getBooksByUsername(username));
    }

    @GetMapping("user/books")
    public ResponseEntity<?> getAllBooksForUser(){
        return ResponseEntity.ok(bookService.getAllBooks());
    }

    @GetMapping("books/buy/{username}/{bookId}")
    public ResponseEntity<?> buyBook(@PathVariable String username, @PathVariable Long bookId){
        try{
            bookService.buyBook(username, bookId);
            return ResponseEntity.ok("Purchased");
        }
        catch (Exception e){
           return new ResponseEntity<>(e, HttpStatus.BAD_REQUEST);
        }
    }
}
