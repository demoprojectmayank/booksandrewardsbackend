package com.rewards.booksandrewards.model.enums;

public enum Role {
    USER,
    ADMIN
}
