package com.rewards.booksandrewards.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Entity
@Table(name = "coupons")
public class Coupon {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name", nullable = false)
    private String couponName;

    @Column(name = "coupon_value", nullable = false)
    private Long couponValue;

    @Column(name = "create_time", nullable = false)
    private LocalDateTime createTime;

    @Column(name = "expiry_time", nullable = false)
    private LocalDateTime expiryTime;

    @Column(name = "username", nullable = false)
    private String username;

    @ManyToOne
    @JsonIgnore
    private User user;
}
