package com.rewards.booksandrewards.model;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Entity
@Table
public class Transaction {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "message")
    private String message;

    @Column(name = "create_time")
    private LocalDateTime createTime;

    @Column(name = "user_id")
    private Long userId;

    public Transaction(String message, Long userId){
        this.message = message;
        this.userId = userId;
        this.createTime = LocalDateTime.now();
    }

    public Transaction() {

    }
}
