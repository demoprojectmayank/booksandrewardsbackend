package com.rewards.booksandrewards.security;

import com.rewards.booksandrewards.model.User;
import com.rewards.booksandrewards.service.UserService;
import com.rewards.booksandrewards.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class CustomUserDetailService implements UserDetailsService {

    @Autowired
    private UserService userService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userService.findByUsername(username).orElseThrow(() -> new UsernameNotFoundException(username));
        Set<GrantedAuthority> authorities = Stream.of(SecurityUtils.convertToAuthority(user.getRole().name())).collect(Collectors.toSet());

        return UserPrincipal
                .builder()
                .user(user)
                .authorities(authorities)
                .id(user.getId())
                .username(username)
                .password(user.getPassword())
                .build();
    }
}
