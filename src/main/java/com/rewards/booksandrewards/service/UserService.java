package com.rewards.booksandrewards.service;

import com.rewards.booksandrewards.model.User;
import com.rewards.booksandrewards.model.enums.Role;

import java.util.List;
import java.util.Optional;

public interface UserService {

    User createNewUser(User user);

    List<User> getAllUser();

    Optional<User> findByUsername(String username);

    void changeRole(Role newRole, String username);

    Optional<User> findByUserId(Long id);

    boolean switchRole(String username);

    boolean rewardTicketToUser(Long couponId, String username1, String username2);
}
