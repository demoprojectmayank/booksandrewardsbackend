package com.rewards.booksandrewards.service;

import com.rewards.booksandrewards.model.Transaction;
import com.rewards.booksandrewards.model.User;
import com.rewards.booksandrewards.repository.TransactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class TransactionServiceImpl implements TransactionService{

    @Autowired
    private TransactionRepository transactionRepository;

    @Autowired
    private UserService userService;

    @Override
    public List<Transaction> getAllTransactions(){
        return transactionRepository.findAll();
    }

    @Override
    public void saveTransaction(Transaction transaction) {
        transactionRepository.save(transaction);
    }

    @Override
    public List<Transaction> getTransactionByUsername(String username){
        Optional<User> user =  userService.findByUsername(username);
        if(!user.isPresent())return new ArrayList<>();

        return transactionRepository.findByUserId(user.get().getId());
    }
}
