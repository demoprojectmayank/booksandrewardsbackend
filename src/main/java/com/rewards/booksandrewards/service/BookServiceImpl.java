package com.rewards.booksandrewards.service;

import com.rewards.booksandrewards.model.Book;
import com.rewards.booksandrewards.model.Transaction;
import com.rewards.booksandrewards.model.User;
import com.rewards.booksandrewards.repository.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class BookServiceImpl implements BookService {

    @Autowired
    private BookRepository bookRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private CouponService couponService;

    @Autowired
    private TransactionService transactionService;

    @Autowired
    private EmailService emailService;

    @Override
    public Book saveBook(Book book) {
        book.setCreateTime(LocalDateTime.now());
        return bookRepository.save(book);
    }

    @Override
    public void updateBookQuantity(Long bookId, Long quantity) {
        Optional<Book> book = bookRepository.findById(bookId);
        if (!book.isPresent()) return;

        book.get().setQuantity(quantity);
        bookRepository.save(book.get());
    }

    @Override
    public void deleteBook(Long bookId) {
        bookRepository.deleteById(bookId);
    }

    @Override
    public List<Book> getAllBooks() {
        return bookRepository.findAll();
    }

    @Override
    public List<Book> getBooksByUsername(String username) {
        Optional<User> user = userService.findByUsername(username);
        if (!user.isPresent()) return new ArrayList<>();

        return user.get().getBooks();
    }

    @Override
    public void buyBook(String username, Long bookId) throws Exception {
        Optional<Book> book = bookRepository.findById(bookId);
        Optional<User> user = userService.findByUsername(username);

        if (!user.isPresent() || !book.isPresent()) throw new Exception("User Or Book Not Found");
        if (book.get().getQuantity() == 0) throw new Exception("Book is not available anymore");
        Long totalCouponValue = couponService.getTotalCouponValueForUsername(username);
        if (totalCouponValue < book.get().getPrice()) throw new Exception("Not Enough Money");

        book.get().setQuantity(book.get().getQuantity() - 1);
        user.get().addBook(book.get());

        couponService.deductAmountFromCoupon(user.get(), book.get().getPrice());

        String message = "Purchase Details: \nBook Name: "
                + book.get().getBookName()
                + "\nBook Price: " + book.get().getPrice();

        transactionService.saveTransaction(new Transaction(message, user.get().getId()));
        emailService.sendSimpleMessage(user.get().getEmail(), "Book Purchase Successful Receipt", message);
        bookRepository.save(book.get());
    }

}
