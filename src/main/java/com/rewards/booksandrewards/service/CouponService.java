package com.rewards.booksandrewards.service;

import com.rewards.booksandrewards.model.Coupon;
import com.rewards.booksandrewards.model.User;

import java.util.List;
import java.util.Optional;

public interface CouponService {

    List<Coupon> getAllCoupons();

    boolean addCoupon(Coupon coupon, String username);

    List<Coupon> getCouponsByUsername(String username);

    Long getTotalCouponValueForUsername(String username);

    void deductAmountFromCoupon(User user, Long price);

    Optional<Coupon> getCouponById(Long couponId);
}
