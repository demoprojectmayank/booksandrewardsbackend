package com.rewards.booksandrewards.service;

import com.rewards.booksandrewards.model.Coupon;
import com.rewards.booksandrewards.model.User;
import com.rewards.booksandrewards.repository.CouponRepository;
import com.rewards.booksandrewards.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CouponServiceImpl implements CouponService {

    @Autowired
    private CouponRepository couponRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private UserRepository userRepository;

    @Override
    public List<Coupon> getAllCoupons() {
        return couponRepository.findAll();
    }

    @Override
    public boolean addCoupon(Coupon coupon, String username) {
        coupon.setCreateTime(LocalDateTime.now());
        coupon.setExpiryTime(LocalDateTime.now().plusSeconds(7 * 24 * 60 * 60));
        Optional<User> user = userService.findByUsername(username);
        if (!user.isPresent()) return false;

        coupon.setUser(user.get());
        coupon.setUsername(user.get().getUsername());
        user.get().addCoupon(coupon);
        userRepository.save(user.get());
        return true;
    }

    @Override
    public List<Coupon> getCouponsByUsername(String username) {
        Optional<User> user = userService.findByUsername(username);
        if (!user.isPresent()) return new ArrayList<>();

        return user.get().getCoupons();
    }

    @Override
    public Long getTotalCouponValueForUsername(String username) {
        Optional<User> user = userService.findByUsername(username);
        Long totalCouponValue = 0L;
        if (!user.isPresent()) return totalCouponValue;

        for (Coupon coupon : user.get().getCoupons()) {
            totalCouponValue += coupon.getCouponValue();
        }
        return totalCouponValue;
    }

    @Override
    public void deductAmountFromCoupon(User user, Long price) {
        List<Coupon> userCoupons = user.getCoupons();
        userCoupons.sort((Coupon a, Coupon b) -> {
            if (a.getExpiryTime().isBefore(b.getExpiryTime())) return -1;
            else return 1;
        });

        int sz = userCoupons.size(); // 2
        for (int i = 0; i < sz && price > 0; i++) {
            Coupon coupon = userCoupons.get(0);
            if (coupon.getExpiryTime().isBefore(LocalDateTime.now())) {
                couponRepository.delete(coupon);
                continue;
            }

            if (price > coupon.getCouponValue()) {
                price -= coupon.getCouponValue();
                user.getCoupons().remove(coupon);
                couponRepository.delete(coupon);
                continue;
            }

            coupon.setCouponValue(coupon.getCouponValue() - price);
            couponRepository.save(coupon);
            price = 0L;
        }
        userRepository.save(user);
    }

    @Override
    public Optional<Coupon> getCouponById(Long couponId) {
        return couponRepository.findById(couponId);
    }
}
