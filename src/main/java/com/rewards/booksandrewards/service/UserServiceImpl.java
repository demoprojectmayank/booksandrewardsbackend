package com.rewards.booksandrewards.service;

import com.rewards.booksandrewards.model.Coupon;
import com.rewards.booksandrewards.model.Transaction;
import com.rewards.booksandrewards.model.User;
import com.rewards.booksandrewards.model.enums.Role;
import com.rewards.booksandrewards.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.swing.text.html.Option;
import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private CouponService couponService;

    @Autowired
    private TransactionService transactionService;

    @Autowired
    private EmailService emailService;

    @Override
    public User createNewUser(User user) {
        user.setCreateTime(LocalDateTime.now());
        user.setRole(Role.USER);
        user.setPassword(passwordEncoder.encode(user.getPassword()));

        User ret = userRepository.save(user);
        for (long i = 1L; i <= 5; i++) {
            Long couponValue = i * 100;
            Coupon coupon = new Coupon();
            coupon.setCouponValue(couponValue);
            coupon.setCouponName("Welcome gift #" + i);

            couponService.addCoupon(coupon, user.getUsername());
        }
        return ret;
    }

    @Override
    public List<User> getAllUser() {
        return userRepository.findAll();
    }

    @Override
    public Optional<User> findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    @Override
    @Transactional
    public void changeRole(Role newRole, String username) {
        userRepository.updateUserRole(username, newRole);
    }

    @Override
    public Optional<User> findByUserId(Long id) {
        return userRepository.findById(id);
    }

    @Override
    public boolean switchRole(String username) {
        Optional<User> user = findByUsername(username);
        if (!user.isPresent()) return false;

        Role role = user.get().getRole();
        user.get().setRole(role == Role.USER ? Role.ADMIN : Role.USER);
        userRepository.save(user.get());
        return true;
    }

    @Override
    public boolean rewardTicketToUser(Long couponId, String username1, String username2) {
        Optional<Coupon> coupon = couponService.getCouponById(couponId);
        Optional<User> user1 = findByUsername(username1);
        Optional<User> user2 = findByUsername(username2);

        if (!coupon.isPresent() || !user1.isPresent() || !user2.isPresent()) return false;
        user1.get().getCoupons().remove(coupon.get());
        coupon.get().setUser(user2.get());
        coupon.get().setUsername(user2.get().getUsername());
        user2.get().addCoupon(coupon.get());

        userRepository.save(user1.get());
        userRepository.save(user2.get());

        String message = "Reward Details: \nReceiver's Username: " + username2
                + "\nCoupon Name: " + coupon.get().getCouponName()
                + "\nCoupon Value: " + coupon.get().getCouponValue();

        transactionService.saveTransaction(new Transaction(message, user1.get().getId()));

        emailService.sendSimpleMessage(user1.get().getEmail(), "Reward sent to " + username2, message);
        return true;
    }
}
