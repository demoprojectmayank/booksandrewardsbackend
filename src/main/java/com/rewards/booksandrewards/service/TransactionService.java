package com.rewards.booksandrewards.service;

import com.rewards.booksandrewards.model.Transaction;

import java.util.List;

public interface TransactionService {
    List<Transaction> getAllTransactions();

    void saveTransaction(Transaction transaction);

    List<Transaction> getTransactionByUsername(String username);
}
