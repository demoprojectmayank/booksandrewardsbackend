package com.rewards.booksandrewards.service;


import com.rewards.booksandrewards.model.User;

public interface AuthenticationService {
    User signInAndReturnJwt(User signInRequest) throws Exception;
}
