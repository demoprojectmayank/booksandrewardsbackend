package com.rewards.booksandrewards.service;

import com.rewards.booksandrewards.model.Book;

import java.util.List;

public interface BookService {

    Book saveBook(Book book);

    void updateBookQuantity(Long bookId, Long quantity);

    void deleteBook(Long bookId);

    List<Book> getAllBooks();

    List<Book> getBooksByUsername(String username);

    void buyBook(String username, Long bookId) throws Exception;
}
