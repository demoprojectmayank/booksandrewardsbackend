package com.rewards.booksandrewards.service;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;

import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class EmailServiceImplTest {

    @Mock
    private JavaMailSender javaMailSender;

    @InjectMocks
    private EmailServiceImpl emailService;

    @Test
    public void sendSimpleMessage() {
        emailService.sendSimpleMessage("someone@gmail.com", "subject", "message body");

        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom("mayankb@zeta.tech");
        message.setTo("someone@gmail.com");
        message.setSubject("subject");
        message.setText("message body");

        verify(javaMailSender).send(message);
    }
}