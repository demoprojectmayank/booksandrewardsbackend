package com.rewards.booksandrewards.service;

import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.rewards.booksandrewards.model.Book;
import com.rewards.booksandrewards.model.User;
import com.rewards.booksandrewards.model.enums.Role;
import com.rewards.booksandrewards.repository.BookRepository;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ContextConfiguration(classes = {BookServiceImpl.class})
@ExtendWith(SpringExtension.class)
class BookServiceImplTest {
    @MockBean
    private BookRepository bookRepository;

    @Autowired
    private BookServiceImpl bookServiceImpl;

    @MockBean
    private CouponService couponService;

    @MockBean
    private EmailService emailService;

    @MockBean
    private TransactionService transactionService;

    @MockBean
    private UserService userService;

    @Test
    void testSaveBook() {
        Book book = new Book();
        book.setAuthorName("JaneDoe");
        book.setBookName("Book Name");
        book.setCreateTime(LocalDateTime.of(1, 1, 1, 1, 1));
        book.setId(123L);
        book.setPrice(1L);
        book.setQuantity(1L);
        book.setUsers(new ArrayList<>());
        when(this.bookRepository.save(any())).thenReturn(book);

        Book book1 = new Book();
        book1.setAuthorName("JaneDoe");
        book1.setBookName("Book Name");
        book1.setCreateTime(LocalDateTime.of(1, 1, 1, 1, 1));
        book1.setId(123L);
        book1.setPrice(1L);
        book1.setQuantity(1L);
        book1.setUsers(new ArrayList<>());
        assertSame(book, this.bookServiceImpl.saveBook(book1));
        verify(this.bookRepository).save(any());
        assertTrue(this.bookServiceImpl.getAllBooks().isEmpty());
    }

    @Test
    void testUpdateBookQuantity() {
        Book book = new Book();
        book.setAuthorName("JaneDoe");
        book.setBookName("Book Name");
        book.setCreateTime(LocalDateTime.of(1, 1, 1, 1, 1));
        book.setId(123L);
        book.setPrice(1L);
        book.setQuantity(1L);
        book.setUsers(new ArrayList<>());
        Optional<Book> ofResult = Optional.of(book);

        Book book1 = new Book();
        book1.setAuthorName("JaneDoe");
        book1.setBookName("Book Name");
        book1.setCreateTime(LocalDateTime.of(1, 1, 1, 1, 1));
        book1.setId(123L);
        book1.setPrice(1L);
        book1.setQuantity(1L);
        book1.setUsers(new ArrayList<>());
        when(this.bookRepository.save(any())).thenReturn(book1);
        when(this.bookRepository.findById(any())).thenReturn(ofResult);
        this.bookServiceImpl.updateBookQuantity(123L, 1L);
        verify(this.bookRepository).save(any());
        verify(this.bookRepository).findById(any());
        assertTrue(this.bookServiceImpl.getAllBooks().isEmpty());
    }

    @Test
    void testUpdateBookQuantity2() {
        Book book = new Book();
        book.setAuthorName("JaneDoe");
        book.setBookName("Book Name");
        book.setCreateTime(LocalDateTime.of(1, 1, 1, 1, 1));
        book.setId(123L);
        book.setPrice(1L);
        book.setQuantity(1L);
        book.setUsers(new ArrayList<>());
        when(this.bookRepository.save(any())).thenReturn(book);
        when(this.bookRepository.findById(any())).thenReturn(Optional.empty());
        this.bookServiceImpl.updateBookQuantity(123L, 1L);
        verify(this.bookRepository).findById(any());
        assertTrue(this.bookServiceImpl.getAllBooks().isEmpty());
    }

    @Test
    void testDeleteBook() {
        doNothing().when(this.bookRepository).deleteById(any());
        this.bookServiceImpl.deleteBook(123L);
        verify(this.bookRepository).deleteById(any());
        assertTrue(this.bookServiceImpl.getAllBooks().isEmpty());
    }

    @Test
    void testGetAllBooks() {
        ArrayList<Book> bookList = new ArrayList<>();
        when(this.bookRepository.findAll()).thenReturn(bookList);
        List<Book> actualAllBooks = this.bookServiceImpl.getAllBooks();
        assertSame(bookList, actualAllBooks);
        assertTrue(actualAllBooks.isEmpty());
        verify(this.bookRepository).findAll();
    }

    @Test
    void testGetBooksByUsername() {
        User user = new User();
        ArrayList<Book> bookList = new ArrayList<>();
        user.setBooks(bookList);
        user.setCoupons(new ArrayList<>());
        user.setCreateTime(LocalDateTime.of(1, 1, 1, 1, 1));
        user.setEmail("jane.doe@example.org");
        user.setId(123L);
        user.setPassword("iloveyou");
        user.setRole(Role.USER);
        user.setToken("ABC123");
        user.setUsername("janedoe");
        Optional<User> ofResult = Optional.of(user);
        when(this.userService.findByUsername(any())).thenReturn(ofResult);
        List<Book> actualBooksByUsername = this.bookServiceImpl.getBooksByUsername("janedoe");
        assertSame(bookList, actualBooksByUsername);
        assertTrue(actualBooksByUsername.isEmpty());
        verify(this.userService).findByUsername(any());
        assertTrue(this.bookServiceImpl.getAllBooks().isEmpty());
    }

    @Test
    void testGetBooksByUsername2() {
        when(this.userService.findByUsername(any())).thenReturn(Optional.empty());
        assertTrue(this.bookServiceImpl.getBooksByUsername("janedoe").isEmpty());
        verify(this.userService).findByUsername(any());
        assertTrue(this.bookServiceImpl.getAllBooks().isEmpty());
    }

    @Test
    void testBuyBook() throws Exception {
        User user = new User();
        user.setBooks(new ArrayList<>());
        user.setCoupons(new ArrayList<>());
        user.setCreateTime(LocalDateTime.of(1, 1, 1, 1, 1));
        user.setEmail("jane.doe@example.org");
        user.setId(123L);
        user.setPassword("iloveyou");
        user.setRole(Role.USER);
        user.setToken("ABC123");
        user.setUsername("janedoe");
        Optional<User> ofResult = Optional.of(user);
        when(this.userService.findByUsername(any())).thenReturn(ofResult);
        doNothing().when(this.transactionService).saveTransaction(any());
        doNothing().when(this.emailService).sendSimpleMessage(any(), any(), any());
        doNothing().when(this.couponService).deductAmountFromCoupon(any(), any());
        when(this.couponService.getTotalCouponValueForUsername(any())).thenReturn(42L);

        Book book = new Book();
        book.setAuthorName("JaneDoe");
        book.setBookName("Book Name");
        book.setCreateTime(LocalDateTime.of(1, 1, 1, 1, 1));
        book.setId(123L);
        book.setPrice(1L);
        book.setQuantity(1L);
        book.setUsers(new ArrayList<>());
        Optional<Book> ofResult1 = Optional.of(book);

        Book book1 = new Book();
        book1.setAuthorName("JaneDoe");
        book1.setBookName("Book Name");
        book1.setCreateTime(LocalDateTime.of(1, 1, 1, 1, 1));
        book1.setId(123L);
        book1.setPrice(1L);
        book1.setQuantity(1L);
        book1.setUsers(new ArrayList<>());
        when(this.bookRepository.save(any())).thenReturn(book1);
        when(this.bookRepository.findById(any())).thenReturn(ofResult1);
        this.bookServiceImpl.buyBook("janedoe", 123L);
        verify(this.userService).findByUsername(any());
        verify(this.transactionService).saveTransaction(any());
        verify(this.emailService).sendSimpleMessage(any(), any(), any());
        verify(this.couponService).getTotalCouponValueForUsername(any());
        verify(this.couponService).deductAmountFromCoupon(any(), any());
        verify(this.bookRepository).save(any());
        verify(this.bookRepository).findById(any());
        assertTrue(this.bookServiceImpl.getAllBooks().isEmpty());
    }

    @Test
    void testBuyBook2() throws Exception {
        when(this.userService.findByUsername(any())).thenReturn(Optional.empty());
        doNothing().when(this.transactionService).saveTransaction(any());
        doNothing().when(this.emailService).sendSimpleMessage(any(), any(), any());
        doNothing().when(this.couponService).deductAmountFromCoupon(any(), any());
        when(this.couponService.getTotalCouponValueForUsername(any())).thenReturn(42L);

        Book book = new Book();
        book.setAuthorName("JaneDoe");
        book.setBookName("Book Name");
        book.setCreateTime(LocalDateTime.of(1, 1, 1, 1, 1));
        book.setId(123L);
        book.setPrice(1L);
        book.setQuantity(1L);
        book.setUsers(new ArrayList<>());
        Optional<Book> ofResult = Optional.of(book);

        Book book1 = new Book();
        book1.setAuthorName("JaneDoe");
        book1.setBookName("Book Name");
        book1.setCreateTime(LocalDateTime.of(1, 1, 1, 1, 1));
        book1.setId(123L);
        book1.setPrice(1L);
        book1.setQuantity(1L);
        book1.setUsers(new ArrayList<>());
        when(this.bookRepository.save(any())).thenReturn(book1);
        when(this.bookRepository.findById(any())).thenReturn(ofResult);
        assertThrows(Exception.class, () -> this.bookServiceImpl.buyBook("janedoe", 123L));
        verify(this.userService).findByUsername(any());
        verify(this.bookRepository).findById(any());
    }

    @Test
    void testBuyBook3() throws Exception {
        User user = new User();
        user.setBooks(new ArrayList<>());
        user.setCoupons(new ArrayList<>());
        user.setCreateTime(LocalDateTime.of(1, 1, 1, 1, 1));
        user.setEmail("jane.doe@example.org");
        user.setId(123L);
        user.setPassword("iloveyou");
        user.setRole(Role.USER);
        user.setToken("ABC123");
        user.setUsername("janedoe");
        Optional<User> ofResult = Optional.of(user);
        when(this.userService.findByUsername(any())).thenReturn(ofResult);
        doNothing().when(this.transactionService).saveTransaction(any());
        doNothing().when(this.emailService).sendSimpleMessage(any(), any(), any());
        doNothing().when(this.couponService).deductAmountFromCoupon(any(), any());
        when(this.couponService.getTotalCouponValueForUsername(any())).thenReturn(0L);

        Book book = new Book();
        book.setAuthorName("JaneDoe");
        book.setBookName("Book Name");
        book.setCreateTime(LocalDateTime.of(1, 1, 1, 1, 1));
        book.setId(123L);
        book.setPrice(1L);
        book.setQuantity(1L);
        book.setUsers(new ArrayList<>());
        Optional<Book> ofResult1 = Optional.of(book);

        Book book1 = new Book();
        book1.setAuthorName("JaneDoe");
        book1.setBookName("Book Name");
        book1.setCreateTime(LocalDateTime.of(1, 1, 1, 1, 1));
        book1.setId(123L);
        book1.setPrice(1L);
        book1.setQuantity(1L);
        book1.setUsers(new ArrayList<>());
        when(this.bookRepository.save(any())).thenReturn(book1);
        when(this.bookRepository.findById(any())).thenReturn(ofResult1);
        assertThrows(Exception.class, () -> this.bookServiceImpl.buyBook("janedoe", 123L));
        verify(this.userService).findByUsername(any());
        verify(this.couponService).getTotalCouponValueForUsername(any());
        verify(this.bookRepository).findById(any());
    }
}

