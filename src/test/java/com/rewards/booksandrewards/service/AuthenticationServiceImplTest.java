package com.rewards.booksandrewards.service;

import com.rewards.booksandrewards.model.User;
import com.rewards.booksandrewards.security.UserPrincipal;
import com.rewards.booksandrewards.security.jwt.JwtProvider;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class AuthenticationServiceImplTest {

    @InjectMocks
    private AuthenticationServiceImpl authenticationService;

    @Mock
    private AuthenticationManager authenticationManager;

    @Mock
    private JwtProvider jwtProvider;

    @Mock
    private Authentication authentication;

    @Mock
    UserPrincipal userPrincipal;

    @Test
    public void signInAndReturnUser() throws Exception {
        User user = new User();
        user.setUsername("testingUsername");
        user.setPassword("password");

        when(authenticationManager.authenticate(any())).thenReturn(authentication);
        when(authentication.getPrincipal()).thenReturn(userPrincipal);
        when(jwtProvider.generateToken(any())).thenReturn("testingjwt");
        when(userPrincipal.getUser()).thenReturn(user);

        User expected = authenticationService.signInAndReturnJwt(user);

        assertThat(expected.getToken()).isEqualTo("testingjwt");
        assertThat(expected.getUsername()).isEqualTo("testingUsername");
    }
}