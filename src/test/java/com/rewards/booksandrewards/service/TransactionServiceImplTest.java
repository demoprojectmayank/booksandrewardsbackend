package com.rewards.booksandrewards.service;

import com.rewards.booksandrewards.model.Transaction;
import com.rewards.booksandrewards.model.User;
import com.rewards.booksandrewards.model.enums.Role;
import com.rewards.booksandrewards.repository.TransactionRepository;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ContextConfiguration(classes = {TransactionServiceImpl.class})
@ExtendWith(MockitoExtension.class)
class TransactionServiceImplTest {

    @InjectMocks
    private TransactionServiceImpl transactionService;

    @Mock
    private UserService userService;

    @Mock
    private TransactionRepository transactionRepository;

    @Test
    public void getTransactionByUsername() {
        User user = new User();
        user.setId(1999L);
        when(userService.findByUsername(any())).thenReturn(Optional.of(user));
        transactionService.getTransactionByUsername("testing");

        verify(transactionRepository).findByUserId(user.getId());
    }

    @Test
    void testGetTransactionByUsername_getList() {
        User user = new User();
        user.setId(123L);

        when(userService.findByUsername(any())).thenReturn(Optional.of(user));

        List<Transaction> transactions = new ArrayList<>();
        when(transactionRepository.findByUserId(any())).thenReturn(transactions);

        List<Transaction> actualTransactionByUsername = transactionService.getTransactionByUsername("testing");
        assertSame(transactions, actualTransactionByUsername);
        assertTrue(actualTransactionByUsername.isEmpty());

        verify(userService).findByUsername(any());
        verify(transactionRepository).findByUserId(any());
        assertTrue(transactionService.getAllTransactions().isEmpty());
    }
}