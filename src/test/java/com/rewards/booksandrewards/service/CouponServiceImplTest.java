package com.rewards.booksandrewards.service;

import com.rewards.booksandrewards.model.Coupon;
import com.rewards.booksandrewards.model.User;
import com.rewards.booksandrewards.model.enums.Role;
import com.rewards.booksandrewards.repository.CouponRepository;
import com.rewards.booksandrewards.repository.UserRepository;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.context.ContextConfiguration;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ContextConfiguration(classes = {CouponServiceImpl.class})
@ExtendWith(MockitoExtension.class)
class CouponServiceImplTest {

    @Mock
    private CouponRepository couponRepository;

    @InjectMocks
    private CouponServiceImpl couponServiceImpl;

    @Mock
    private UserRepository userRepository;

    @Mock
    private UserService userService;

    @Test
    void testGetAllCoupons() {
        ArrayList<Coupon> couponList = new ArrayList<>();
        when(this.couponRepository.findAll()).thenReturn(couponList);
        List<Coupon> actualAllCoupons = this.couponServiceImpl.getAllCoupons();
        assertSame(couponList, actualAllCoupons);
        assertTrue(actualAllCoupons.isEmpty());
        verify(this.couponRepository).findAll();
    }

    @Test
    void testAddCoupon() {
        User user = new User();
        user.setId(123L);
        user.setUsername("testingUsername");

        when(userService.findByUsername(any())).thenReturn(Optional.of(user));
        when(userRepository.save(any())).thenReturn(user);

        Coupon coupon = new Coupon();
        coupon.setCouponName("Coupon Name");
        coupon.setCouponValue(42L);
        coupon.setId(123L);

        assertTrue(couponServiceImpl.addCoupon(coupon, "testing"));
        verify(userService).findByUsername(any());
        verify(userRepository).save(any());
        assertEquals("testingUsername", coupon.getUsername());

        User user2 = coupon.getUser();
        assertSame(user, user2);
        assertEquals(1, user2.getCoupons().size());

        assertTrue(couponServiceImpl.getAllCoupons().isEmpty());
    }

    @Test
    void testAddCoupon2() {
        when(this.userService.findByUsername(any())).thenReturn(Optional.empty());

        User user = new User();
        user.setId(123L);

        Coupon coupon = new Coupon();
        coupon.setCouponName("Coupon Name");
        coupon.setCouponValue(42L);

        assertFalse(couponServiceImpl.addCoupon(coupon, "janedoe"));
        verify(userService).findByUsername(any());
    }

    @Test
    void testAddCoupon3() {
        User user = new User();
        user.setBooks(new ArrayList<>());
        user.setCoupons(new ArrayList<>());
        user.setCreateTime(LocalDateTime.of(1, 1, 1, 1, 1));
        user.setEmail("jane.doe@example.org");
        user.setId(123L);
        user.setPassword("iloveyou");
        user.setRole(Role.USER);
        user.setToken("ABC123");
        user.setUsername("janedoe");
        Optional<User> ofResult = Optional.of(user);
        when(this.userService.findByUsername(any())).thenReturn(ofResult);

        User user1 = new User();
        user1.setBooks(new ArrayList<>());
        user1.setCoupons(new ArrayList<>());
        user1.setCreateTime(LocalDateTime.of(1, 1, 1, 1, 1));
        user1.setEmail("jane.doe@example.org");
        user1.setId(123L);
        user1.setPassword("iloveyou");
        user1.setRole(Role.USER);
        user1.setToken("ABC123");
        user1.setUsername("janedoe");
        when(this.userRepository.save(any())).thenReturn(user1);

        User user2 = new User();
        user2.setBooks(new ArrayList<>());
        user2.setCoupons(new ArrayList<>());
        user2.setCreateTime(LocalDateTime.of(1, 1, 1, 1, 1));
        user2.setEmail("jane.doe@example.org");
        user2.setId(123L);
        user2.setPassword("iloveyou");
        user2.setRole(Role.USER);
        user2.setToken("ABC123");
        user2.setUsername("janedoe");

        Coupon coupon = new Coupon();
        coupon.setCouponName("Coupon Name");
        coupon.setCouponValue(42L);
        coupon.setCreateTime(LocalDateTime.of(1, 1, 1, 1, 1));
        coupon.setExpiryTime(LocalDateTime.of(1, 1, 1, 1, 1));
        coupon.setId(123L);
        coupon.setUser(user2);
        coupon.setUsername("janedoe");
        assertTrue(this.couponServiceImpl.addCoupon(coupon, "janedoe"));
        verify(this.userService).findByUsername(any());
        verify(this.userRepository).save(any());
        assertEquals("janedoe", coupon.getUsername());
        User user3 = coupon.getUser();
        assertSame(user, user3);
        assertEquals(1, user3.getCoupons().size());
        assertTrue(this.couponServiceImpl.getAllCoupons().isEmpty());
    }

    @Test
    void testAddCoupon4() {
        when(this.userService.findByUsername(any())).thenReturn(Optional.empty());

        User user = new User();
        user.setBooks(new ArrayList<>());
        user.setCoupons(new ArrayList<>());
        user.setCreateTime(LocalDateTime.of(1, 1, 1, 1, 1));
        user.setEmail("jane.doe@example.org");
        user.setId(123L);
        user.setPassword("iloveyou");
        user.setRole(Role.USER);
        user.setToken("ABC123");
        user.setUsername("janedoe");

        User user1 = new User();
        user1.setBooks(new ArrayList<>());
        user1.setCoupons(new ArrayList<>());
        user1.setCreateTime(LocalDateTime.of(1, 1, 1, 1, 1));
        user1.setEmail("jane.doe@example.org");
        user1.setId(123L);
        user1.setPassword("iloveyou");
        user1.setRole(Role.USER);
        user1.setToken("ABC123");
        user1.setUsername("janedoe");

        Coupon coupon = new Coupon();
        coupon.setCouponName("Coupon Name");
        coupon.setCouponValue(42L);
        coupon.setCreateTime(LocalDateTime.of(1, 1, 1, 1, 1));
        coupon.setExpiryTime(LocalDateTime.of(1, 1, 1, 1, 1));
        coupon.setId(123L);
        coupon.setUser(user1);
        coupon.setUsername("janedoe");
        assertFalse(this.couponServiceImpl.addCoupon(coupon, "janedoe"));
        verify(this.userService).findByUsername(any());
        assertTrue(this.couponServiceImpl.getAllCoupons().isEmpty());
    }

    @Test
    void testGetCouponsByUsername() {
        User user = new User();
        user.setBooks(new ArrayList<>());
        ArrayList<Coupon> couponList = new ArrayList<>();
        user.setCoupons(couponList);
        user.setId(123L);
        user.setUsername("testingUsername");

        when(userService.findByUsername(any())).thenReturn(Optional.of(user));

        List<Coupon> actualCouponsByUsername = couponServiceImpl.getCouponsByUsername("testingUsername");
        assertSame(couponList, actualCouponsByUsername);
        assertTrue(actualCouponsByUsername.isEmpty());
        verify(userService).findByUsername(any());
        assertTrue(couponServiceImpl.getAllCoupons().isEmpty());
    }

    @Test
    void testGetCouponsByUsername2() {
        when(this.userService.findByUsername(any())).thenReturn(Optional.empty());
        assertTrue(this.couponServiceImpl.getCouponsByUsername("janedoe").isEmpty());
        verify(this.userService).findByUsername(any());
        assertTrue(this.couponServiceImpl.getAllCoupons().isEmpty());
    }

    @Test
    void testGetCouponsByUsername3() {
        User user = new User();
        user.setBooks(new ArrayList<>());
        ArrayList<Coupon> couponList = new ArrayList<>();
        user.setCoupons(couponList);
        user.setCreateTime(LocalDateTime.of(1, 1, 1, 1, 1));
        user.setEmail("jane.doe@example.org");
        user.setId(123L);
        user.setPassword("iloveyou");
        user.setRole(Role.USER);
        user.setToken("ABC123");
        user.setUsername("janedoe");
        Optional<User> ofResult = Optional.of(user);
        when(this.userService.findByUsername(any())).thenReturn(ofResult);
        List<Coupon> actualCouponsByUsername = this.couponServiceImpl.getCouponsByUsername("janedoe");
        assertSame(couponList, actualCouponsByUsername);
        assertTrue(actualCouponsByUsername.isEmpty());
        verify(this.userService).findByUsername(any());
        assertTrue(this.couponServiceImpl.getAllCoupons().isEmpty());
    }

    @Test
    void testGetCouponsByUsername4() {
        when(this.userService.findByUsername(any())).thenReturn(Optional.empty());
        assertTrue(this.couponServiceImpl.getCouponsByUsername("janedoe").isEmpty());
        verify(this.userService).findByUsername(any());
        assertTrue(this.couponServiceImpl.getAllCoupons().isEmpty());
    }

    @Test
    void testGetTotalCouponValueForUsername() {
        User user = new User();
        user.setBooks(new ArrayList<>());
        user.setCoupons(new ArrayList<>());
        user.setCreateTime(LocalDateTime.of(1, 1, 1, 1, 1));
        user.setEmail("jane.doe@example.org");
        user.setId(123L);
        user.setPassword("iloveyou");
        user.setRole(Role.USER);
        user.setToken("ABC123");
        user.setUsername("janedoe");
        Optional<User> ofResult = Optional.of(user);
        when(this.userService.findByUsername(any())).thenReturn(ofResult);
        assertEquals(0L, this.couponServiceImpl.getTotalCouponValueForUsername("janedoe").longValue());
        verify(this.userService).findByUsername(any());
        assertTrue(this.couponServiceImpl.getAllCoupons().isEmpty());
    }

    @Test
    void testGetTotalCouponValueForUsername2() {
        User user = new User();
        user.setBooks(new ArrayList<>());
        user.setCoupons(new ArrayList<>());
        user.setCreateTime(LocalDateTime.of(1, 1, 1, 1, 1));
        user.setEmail("jane.doe@example.org");
        user.setId(123L);
        user.setPassword("iloveyou");
        user.setRole(Role.USER);
        user.setToken("ABC123");
        user.setUsername("janedoe");

        Coupon coupon = new Coupon();
        coupon.setCouponName("Coupon Name");
        coupon.setCouponValue(42L);
        coupon.setCreateTime(LocalDateTime.of(1, 1, 1, 1, 1));
        coupon.setExpiryTime(LocalDateTime.of(1, 1, 1, 1, 1));
        coupon.setId(123L);
        coupon.setUser(user);
        coupon.setUsername("janedoe");

        ArrayList<Coupon> couponList = new ArrayList<>();
        couponList.add(coupon);

        User user1 = new User();
        user1.setBooks(new ArrayList<>());
        user1.setCoupons(couponList);
        user1.setCreateTime(LocalDateTime.of(1, 1, 1, 1, 1));
        user1.setEmail("jane.doe@example.org");
        user1.setId(123L);
        user1.setPassword("iloveyou");
        user1.setRole(Role.USER);
        user1.setToken("ABC123");
        user1.setUsername("janedoe");
        Optional<User> ofResult = Optional.of(user1);
        when(this.userService.findByUsername(any())).thenReturn(ofResult);
        assertEquals(42L, this.couponServiceImpl.getTotalCouponValueForUsername("janedoe").longValue());
        verify(this.userService).findByUsername(any());
        assertTrue(this.couponServiceImpl.getAllCoupons().isEmpty());
    }

    @Test
    void testGetTotalCouponValueForUsername3() {
        when(this.userService.findByUsername(any())).thenReturn(Optional.empty());
        assertEquals(0L, this.couponServiceImpl.getTotalCouponValueForUsername("janedoe").longValue());
        verify(this.userService).findByUsername(any());
        assertTrue(this.couponServiceImpl.getAllCoupons().isEmpty());
    }

    @Test
    void testGetTotalCouponValueForUsername4() {
        User user = new User();
        user.setBooks(new ArrayList<>());
        user.setCoupons(new ArrayList<>());
        user.setCreateTime(LocalDateTime.of(1, 1, 1, 1, 1));
        user.setEmail("jane.doe@example.org");
        user.setId(123L);
        user.setPassword("iloveyou");
        user.setRole(Role.USER);
        user.setToken("ABC123");
        user.setUsername("janedoe");
        Optional<User> ofResult = Optional.of(user);
        when(this.userService.findByUsername(any())).thenReturn(ofResult);
        assertEquals(0L, this.couponServiceImpl.getTotalCouponValueForUsername("janedoe").longValue());
        verify(this.userService).findByUsername(any());
        assertTrue(this.couponServiceImpl.getAllCoupons().isEmpty());
    }

    @Test
    void testGetTotalCouponValueForUsername5() {
        User user = new User();
        user.setBooks(new ArrayList<>());
        user.setCoupons(new ArrayList<>());
        user.setCreateTime(LocalDateTime.of(1, 1, 1, 1, 1));
        user.setEmail("jane.doe@example.org");
        user.setId(123L);
        user.setPassword("iloveyou");
        user.setRole(Role.USER);
        user.setToken("ABC123");
        user.setUsername("janedoe");

        Coupon coupon = new Coupon();
        coupon.setCouponName("Coupon Name");
        coupon.setCouponValue(42L);
        coupon.setCreateTime(LocalDateTime.of(1, 1, 1, 1, 1));
        coupon.setExpiryTime(LocalDateTime.of(1, 1, 1, 1, 1));
        coupon.setId(123L);
        coupon.setUser(user);
        coupon.setUsername("janedoe");

        ArrayList<Coupon> couponList = new ArrayList<>();
        couponList.add(coupon);

        User user1 = new User();
        user1.setBooks(new ArrayList<>());
        user1.setCoupons(couponList);
        user1.setCreateTime(LocalDateTime.of(1, 1, 1, 1, 1));
        user1.setEmail("jane.doe@example.org");
        user1.setId(123L);
        user1.setPassword("iloveyou");
        user1.setRole(Role.USER);
        user1.setToken("ABC123");
        user1.setUsername("janedoe");
        Optional<User> ofResult = Optional.of(user1);
        when(this.userService.findByUsername(any())).thenReturn(ofResult);
        assertEquals(42L, this.couponServiceImpl.getTotalCouponValueForUsername("janedoe").longValue());
        verify(this.userService).findByUsername(any());
        assertTrue(this.couponServiceImpl.getAllCoupons().isEmpty());
    }

    @Test
    void testGetTotalCouponValueForUsername6() {
        when(this.userService.findByUsername(any())).thenReturn(Optional.empty());
        assertEquals(0L, this.couponServiceImpl.getTotalCouponValueForUsername("janedoe").longValue());
        verify(this.userService).findByUsername(any());
        assertTrue(this.couponServiceImpl.getAllCoupons().isEmpty());
    }

    @Test
    void testDeductAmountFromCoupon() {
        User user = new User();
        user.setBooks(new ArrayList<>());
        user.setCoupons(new ArrayList<>());
        user.setCreateTime(LocalDateTime.of(1, 1, 1, 1, 1));
        user.setEmail("jane.doe@example.org");
        user.setId(123L);
        user.setPassword("iloveyou");
        user.setRole(Role.USER);
        user.setToken("ABC123");
        user.setUsername("janedoe");
        when(this.userRepository.save(any())).thenReturn(user);

        User user1 = new User();
        user1.setBooks(new ArrayList<>());
        user1.setCoupons(new ArrayList<>());
        user1.setCreateTime(LocalDateTime.of(1, 1, 1, 1, 1));
        user1.setEmail("jane.doe@example.org");
        user1.setId(123L);
        user1.setPassword("iloveyou");
        user1.setRole(Role.USER);
        user1.setToken("ABC123");
        user1.setUsername("janedoe");
        this.couponServiceImpl.deductAmountFromCoupon(user1, 1L);
        verify(this.userRepository).save(any());
        assertTrue(user1.getCoupons().isEmpty());
        assertTrue(this.couponServiceImpl.getAllCoupons().isEmpty());
    }

    @Test
    void testDeductAmountFromCoupon2() {
        User user = new User();
        user.setBooks(new ArrayList<>());
        user.setCoupons(new ArrayList<>());
        user.setCreateTime(LocalDateTime.of(1, 1, 1, 1, 1));
        user.setEmail("jane.doe@example.org");
        user.setId(123L);
        user.setPassword("iloveyou");
        user.setRole(Role.USER);
        user.setToken("ABC123");
        user.setUsername("janedoe");
        when(this.userRepository.save(any())).thenReturn(user);
        doNothing().when(this.couponRepository).delete(any());

        User user1 = new User();
        user1.setBooks(new ArrayList<>());
        user1.setCoupons(new ArrayList<>());
        user1.setCreateTime(LocalDateTime.of(1, 1, 1, 1, 1));
        user1.setEmail("jane.doe@example.org");
        user1.setId(123L);
        user1.setPassword("iloveyou");
        user1.setRole(Role.USER);
        user1.setToken("ABC123");
        user1.setUsername("janedoe");

        Coupon coupon = new Coupon();
        coupon.setCouponName("Coupon Name");
        coupon.setCouponValue(42L);
        coupon.setCreateTime(LocalDateTime.of(1, 1, 1, 1, 1));
        coupon.setExpiryTime(LocalDateTime.of(1, 1, 1, 1, 1));
        coupon.setId(123L);
        coupon.setUser(user1);
        coupon.setUsername("janedoe");

        ArrayList<Coupon> couponList = new ArrayList<>();
        couponList.add(coupon);

        User user2 = new User();
        user2.setBooks(new ArrayList<>());
        user2.setCoupons(couponList);
        user2.setCreateTime(LocalDateTime.of(1, 1, 1, 1, 1));
        user2.setEmail("jane.doe@example.org");
        user2.setId(123L);
        user2.setPassword("iloveyou");
        user2.setRole(Role.USER);
        user2.setToken("ABC123");
        user2.setUsername("janedoe");
        this.couponServiceImpl.deductAmountFromCoupon(user2, 1L);
        verify(this.userRepository).save(any());
        verify(this.couponRepository).delete(any());
        assertEquals(1, user2.getCoupons().size());
        assertTrue(this.couponServiceImpl.getAllCoupons().isEmpty());
    }

    @Test
    void testDeductAmountFromCoupon3() {
        User user = new User();
        user.setBooks(new ArrayList<>());
        user.setCoupons(new ArrayList<>());
        user.setCreateTime(LocalDateTime.of(1, 1, 1, 1, 1));
        user.setEmail("jane.doe@example.org");
        user.setId(123L);
        user.setPassword("iloveyou");
        user.setRole(Role.USER);
        user.setToken("ABC123");
        user.setUsername("janedoe");
        when(this.userRepository.save(any())).thenReturn(user);
        doNothing().when(this.couponRepository).delete(any());

        User user1 = new User();
        user1.setBooks(new ArrayList<>());
        user1.setCoupons(new ArrayList<>());
        user1.setCreateTime(LocalDateTime.of(1, 1, 1, 1, 1));
        user1.setEmail("jane.doe@example.org");
        user1.setId(123L);
        user1.setPassword("iloveyou");
        user1.setRole(Role.USER);
        user1.setToken("ABC123");
        user1.setUsername("janedoe");

        Coupon coupon = new Coupon();
        coupon.setCouponName("Coupon Name");
        coupon.setCouponValue(42L);
        coupon.setCreateTime(LocalDateTime.of(1, 1, 1, 1, 1));
        coupon.setExpiryTime(LocalDateTime.of(1, 1, 1, 1, 1));
        coupon.setId(123L);
        coupon.setUser(user1);
        coupon.setUsername("janedoe");

        User user2 = new User();
        user2.setBooks(new ArrayList<>());
        user2.setCoupons(new ArrayList<>());
        user2.setCreateTime(LocalDateTime.of(1, 1, 1, 1, 1));
        user2.setEmail("jane.doe@example.org");
        user2.setId(123L);
        user2.setPassword("iloveyou");
        user2.setRole(Role.USER);
        user2.setToken("ABC123");
        user2.setUsername("janedoe");

        Coupon coupon1 = new Coupon();
        coupon1.setCouponName("Coupon Name");
        coupon1.setCouponValue(42L);
        coupon1.setCreateTime(LocalDateTime.of(1, 1, 1, 1, 1));
        coupon1.setExpiryTime(LocalDateTime.of(1, 1, 1, 1, 1));
        coupon1.setId(123L);
        coupon1.setUser(user2);
        coupon1.setUsername("janedoe");

        ArrayList<Coupon> couponList = new ArrayList<>();
        couponList.add(coupon1);
        couponList.add(coupon);

        User user3 = new User();
        user3.setBooks(new ArrayList<>());
        user3.setCoupons(couponList);
        user3.setCreateTime(LocalDateTime.of(1, 1, 1, 1, 1));
        user3.setEmail("jane.doe@example.org");
        user3.setId(123L);
        user3.setPassword("iloveyou");
        user3.setRole(Role.USER);
        user3.setToken("ABC123");
        user3.setUsername("janedoe");
        this.couponServiceImpl.deductAmountFromCoupon(user3, 1L);
        verify(this.userRepository).save(any());
        verify(this.couponRepository, atLeast(1)).delete(any());
        assertEquals(2, user3.getCoupons().size());
        assertTrue(this.couponServiceImpl.getAllCoupons().isEmpty());
    }

    @Test
    void testDeductAmountFromCoupon4() {
        User user = new User();
        user.setBooks(new ArrayList<>());
        user.setCoupons(new ArrayList<>());
        user.setCreateTime(LocalDateTime.of(1, 1, 1, 1, 1));
        user.setEmail("jane.doe@example.org");
        user.setId(123L);
        user.setPassword("iloveyou");
        user.setRole(Role.USER);
        user.setToken("ABC123");
        user.setUsername("janedoe");
        when(this.userRepository.save(any())).thenReturn(user);

        User user1 = new User();
        user1.setBooks(new ArrayList<>());
        user1.setCoupons(new ArrayList<>());
        user1.setCreateTime(LocalDateTime.of(1, 1, 1, 1, 1));
        user1.setEmail("jane.doe@example.org");
        user1.setId(123L);
        user1.setPassword("iloveyou");
        user1.setRole(Role.USER);
        user1.setToken("ABC123");
        user1.setUsername("janedoe");
        this.couponServiceImpl.deductAmountFromCoupon(user1, 1L);
        verify(this.userRepository).save(any());
        assertTrue(user1.getCoupons().isEmpty());
        assertTrue(this.couponServiceImpl.getAllCoupons().isEmpty());
    }

    @Test
    void testDeductAmountFromCoupon5() {
        User user = new User();
        user.setBooks(new ArrayList<>());
        user.setCoupons(new ArrayList<>());
        user.setCreateTime(LocalDateTime.of(1, 1, 1, 1, 1));
        user.setEmail("jane.doe@example.org");
        user.setId(123L);
        user.setPassword("iloveyou");
        user.setRole(Role.USER);
        user.setToken("ABC123");
        user.setUsername("janedoe");
        when(this.userRepository.save(any())).thenReturn(user);
        doNothing().when(this.couponRepository).delete(any());

        User user1 = new User();
        user1.setBooks(new ArrayList<>());
        user1.setCoupons(new ArrayList<>());
        user1.setCreateTime(LocalDateTime.of(1, 1, 1, 1, 1));
        user1.setEmail("jane.doe@example.org");
        user1.setId(123L);
        user1.setPassword("iloveyou");
        user1.setRole(Role.USER);
        user1.setToken("ABC123");
        user1.setUsername("janedoe");

        Coupon coupon = new Coupon();
        coupon.setCouponName("Coupon Name");
        coupon.setCouponValue(42L);
        coupon.setCreateTime(LocalDateTime.of(1, 1, 1, 1, 1));
        coupon.setExpiryTime(LocalDateTime.of(1, 1, 1, 1, 1));
        coupon.setId(123L);
        coupon.setUser(user1);
        coupon.setUsername("janedoe");

        ArrayList<Coupon> couponList = new ArrayList<>();
        couponList.add(coupon);

        User user2 = new User();
        user2.setBooks(new ArrayList<>());
        user2.setCoupons(couponList);
        user2.setCreateTime(LocalDateTime.of(1, 1, 1, 1, 1));
        user2.setEmail("jane.doe@example.org");
        user2.setId(123L);
        user2.setPassword("iloveyou");
        user2.setRole(Role.USER);
        user2.setToken("ABC123");
        user2.setUsername("janedoe");
        this.couponServiceImpl.deductAmountFromCoupon(user2, 1L);
        verify(this.userRepository).save(any());
        verify(this.couponRepository).delete(any());
        assertEquals(1, user2.getCoupons().size());
        assertTrue(this.couponServiceImpl.getAllCoupons().isEmpty());
    }

    @Test
    void testDeductAmountFromCoupon6() {
        User user = new User();
        user.setBooks(new ArrayList<>());
        user.setCoupons(new ArrayList<>());
        user.setCreateTime(LocalDateTime.of(1, 1, 1, 1, 1));
        user.setEmail("jane.doe@example.org");
        user.setId(123L);
        user.setPassword("iloveyou");
        user.setRole(Role.USER);
        user.setToken("ABC123");
        user.setUsername("janedoe");
        when(this.userRepository.save(any())).thenReturn(user);
        doNothing().when(this.couponRepository).delete(any());

        User user1 = new User();
        user1.setBooks(new ArrayList<>());
        user1.setCoupons(new ArrayList<>());
        user1.setCreateTime(LocalDateTime.of(1, 1, 1, 1, 1));
        user1.setEmail("jane.doe@example.org");
        user1.setId(123L);
        user1.setPassword("iloveyou");
        user1.setRole(Role.USER);
        user1.setToken("ABC123");
        user1.setUsername("janedoe");

        Coupon coupon = new Coupon();
        coupon.setCouponName("Coupon Name");
        coupon.setCouponValue(42L);
        coupon.setCreateTime(LocalDateTime.of(1, 1, 1, 1, 1));
        coupon.setExpiryTime(LocalDateTime.of(1, 1, 1, 1, 1));
        coupon.setId(123L);
        coupon.setUser(user1);
        coupon.setUsername("janedoe");

        User user2 = new User();
        user2.setBooks(new ArrayList<>());
        user2.setCoupons(new ArrayList<>());
        user2.setCreateTime(LocalDateTime.of(1, 1, 1, 1, 1));
        user2.setEmail("jane.doe@example.org");
        user2.setId(123L);
        user2.setPassword("iloveyou");
        user2.setRole(Role.USER);
        user2.setToken("ABC123");
        user2.setUsername("janedoe");

        Coupon coupon1 = new Coupon();
        coupon1.setCouponName("Coupon Name");
        coupon1.setCouponValue(42L);
        coupon1.setCreateTime(LocalDateTime.of(1, 1, 1, 1, 1));
        coupon1.setExpiryTime(LocalDateTime.of(1, 1, 1, 1, 1));
        coupon1.setId(123L);
        coupon1.setUser(user2);
        coupon1.setUsername("janedoe");

        ArrayList<Coupon> couponList = new ArrayList<>();
        couponList.add(coupon1);
        couponList.add(coupon);

        User user3 = new User();
        user3.setBooks(new ArrayList<>());
        user3.setCoupons(couponList);
        user3.setCreateTime(LocalDateTime.of(1, 1, 1, 1, 1));
        user3.setEmail("jane.doe@example.org");
        user3.setId(123L);
        user3.setPassword("iloveyou");
        user3.setRole(Role.USER);
        user3.setToken("ABC123");
        user3.setUsername("janedoe");
        this.couponServiceImpl.deductAmountFromCoupon(user3, 1L);
        verify(this.userRepository).save(any());
        verify(this.couponRepository, atLeast(1)).delete(any());
        assertEquals(2, user3.getCoupons().size());
        assertTrue(this.couponServiceImpl.getAllCoupons().isEmpty());
    }

    @Test
    void testGetCouponById() {
        User user = new User();
        user.setBooks(new ArrayList<>());
        user.setCoupons(new ArrayList<>());
        user.setCreateTime(LocalDateTime.of(1, 1, 1, 1, 1));
        user.setEmail("jane.doe@example.org");
        user.setId(123L);
        user.setPassword("iloveyou");
        user.setRole(Role.USER);
        user.setToken("ABC123");
        user.setUsername("janedoe");

        Coupon coupon = new Coupon();
        coupon.setCouponName("Coupon Name");
        coupon.setCouponValue(42L);
        coupon.setCreateTime(LocalDateTime.of(1, 1, 1, 1, 1));
        coupon.setExpiryTime(LocalDateTime.of(1, 1, 1, 1, 1));
        coupon.setId(123L);
        coupon.setUser(user);
        coupon.setUsername("janedoe");
        Optional<Coupon> ofResult = Optional.of(coupon);
        when(this.couponRepository.findById(any())).thenReturn(ofResult);
        Optional<Coupon> actualCouponById = this.couponServiceImpl.getCouponById(123L);
        assertSame(ofResult, actualCouponById);
        assertTrue(actualCouponById.isPresent());
        verify(this.couponRepository).findById(any());
        assertTrue(this.couponServiceImpl.getAllCoupons().isEmpty());
    }

}