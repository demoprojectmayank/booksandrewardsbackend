package com.rewards.booksandrewards.service;

import com.rewards.booksandrewards.model.Coupon;
import com.rewards.booksandrewards.model.User;
import com.rewards.booksandrewards.model.enums.Role;
import com.rewards.booksandrewards.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.crypto.password.PasswordEncoder;


import java.time.LocalDateTime;
import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class UserServiceImplTest {

    @InjectMocks
    private UserServiceImpl userService;

    @Mock
    private UserRepository userRepository;

    @Mock
    private PasswordEncoder passwordEncoder;

    @Mock
    CouponService couponService;

    @Mock
    EmailService emailService;

    @Mock
    TransactionService transactionService;

    private User user;

    @BeforeEach
    void setUp() {
        user = new User();
        user.setPassword("testing");
        user.setRole(Role.USER);

        when(userRepository.save(any())).thenReturn(user);
    }

    @Test
    public void createNewUser() {
        when(passwordEncoder.encode(any())).thenReturn("testing");

        userService.createNewUser(user);

        assertThat(user.getRole()).isEqualTo(Role.USER);
        assertThat(user.getPassword()).isEqualTo("testing");
    }

    @Test
    public void switchRole() {
        when(userRepository.findByUsername(any())).thenReturn(Optional.of(user));

        assertThat(userService.switchRole("testing")).isTrue();
        assertThat(user.getRole()).isEqualTo(Role.ADMIN);

        when(userRepository.findByUsername(any())).thenReturn(Optional.empty());
        assertThat(userService.switchRole("testing")).isFalse();
    }

    @Test
    public void rewardTicketToUser() {
        Coupon coupon = new Coupon();
        coupon.setCouponName("Random Testing Coupon");
        coupon.setCouponValue(2344L);
        coupon.setUser(user);
        coupon.setCreateTime(LocalDateTime.now());

        user.addCoupon(coupon);

        User user2 = new User();

        when(couponService.getCouponById(anyLong())).thenReturn(Optional.of(coupon));
        when(userRepository.findByUsername(any()))
                .thenReturn(Optional.of(user))
                .thenReturn(Optional.of(user2));

        assertThat(userService.rewardTicketToUser(2344L, "testing", "testing"))
                .isTrue();

        assertThat(user2.getCoupons().contains(coupon)).isTrue();
        assertThat(user.getCoupons().contains(coupon)).isFalse();
        assertThat(coupon.getUser()).isEqualTo(user2);
    }
}