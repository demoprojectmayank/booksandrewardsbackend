package com.rewards.booksandrewards.security;

import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

class UserPrincipalTest {
    @Test
    void testConstructor() {
        UserPrincipal actualUserPrincipal = new UserPrincipal();
        assertNull(actualUserPrincipal.getAuthorities());
        assertNull(actualUserPrincipal.getPassword());
        assertNull(actualUserPrincipal.getUsername());
        assertTrue(actualUserPrincipal.isAccountNonExpired());
        assertTrue(actualUserPrincipal.isAccountNonLocked());
        assertTrue(actualUserPrincipal.isCredentialsNonExpired());
        assertTrue(actualUserPrincipal.isEnabled());
    }
}

