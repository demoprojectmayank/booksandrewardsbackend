package com.rewards.booksandrewards.security.jwt;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.web.multipart.support.DefaultMultipartHttpServletRequest;

class JwtProviderImplTest {
    @Test
    void testGetAuthentication() {
        JwtProviderImpl jwtProviderImpl = new JwtProviderImpl();
        assertNull(jwtProviderImpl.getAuthentication(new MockHttpServletRequest()));
    }

    @Test
    void testGetAuthentication2() {
        JwtProviderImpl jwtProviderImpl = new JwtProviderImpl();
        DefaultMultipartHttpServletRequest defaultMultipartHttpServletRequest = mock(
                DefaultMultipartHttpServletRequest.class);
        when(defaultMultipartHttpServletRequest.getHeader(any())).thenReturn("https://example.org/example");
        assertNull(jwtProviderImpl.getAuthentication(defaultMultipartHttpServletRequest));
        verify(defaultMultipartHttpServletRequest).getHeader(any());
    }

    @Test
    void testIsTokenValid() {
        JwtProviderImpl jwtProviderImpl = new JwtProviderImpl();
        assertFalse(jwtProviderImpl.isTokenValid(new MockHttpServletRequest()));
    }

    @Test
    void testIsTokenValid2() {
        JwtProviderImpl jwtProviderImpl = new JwtProviderImpl();
        DefaultMultipartHttpServletRequest defaultMultipartHttpServletRequest = mock(
                DefaultMultipartHttpServletRequest.class);
        when(defaultMultipartHttpServletRequest.getHeader(any())).thenReturn("https://example.org/example");
        assertFalse(jwtProviderImpl.isTokenValid(defaultMultipartHttpServletRequest));
        verify(defaultMultipartHttpServletRequest).getHeader(any());
    }

    @Test
    void testExtractClaims() {
        JwtProviderImpl jwtProviderImpl = new JwtProviderImpl();
        assertNull(jwtProviderImpl.extractClaims(new MockHttpServletRequest()));
    }

    @Test
    void testExtractClaims2() {
        JwtProviderImpl jwtProviderImpl = new JwtProviderImpl();
        DefaultMultipartHttpServletRequest defaultMultipartHttpServletRequest = mock(
                DefaultMultipartHttpServletRequest.class);
        when(defaultMultipartHttpServletRequest.getHeader(any())).thenReturn("https://example.org/example");
        assertNull(jwtProviderImpl.extractClaims(defaultMultipartHttpServletRequest));
        verify(defaultMultipartHttpServletRequest).getHeader(any());
    }
}

