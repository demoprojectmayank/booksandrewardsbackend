package com.rewards.booksandrewards.security;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.rewards.booksandrewards.model.Book;
import com.rewards.booksandrewards.model.Coupon;
import com.rewards.booksandrewards.model.User;
import com.rewards.booksandrewards.model.enums.Role;
import com.rewards.booksandrewards.service.UserService;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ContextConfiguration(classes = {CustomUserDetailService.class})
@ExtendWith(SpringExtension.class)
class CustomUserDetailServiceTest {
    @Autowired
    private CustomUserDetailService customUserDetailService;

    @MockBean
    private UserService userService;

    @Test
    void testLoadUserByUsername() throws UsernameNotFoundException {
        User user = new User();
        user.setBooks(new ArrayList<>());
        user.setCoupons(new ArrayList<>());
        user.setCreateTime(LocalDateTime.of(1, 1, 1, 1, 1));
        user.setEmail("jane.doe@example.org");
        user.setId(123L);
        user.setPassword("iloveyou");
        user.setRole(Role.USER);
        user.setToken("ABC123");
        user.setUsername("janedoe");
        Optional<User> ofResult = Optional.of(user);
        when(this.userService.findByUsername(any())).thenReturn(ofResult);
        UserDetails actualLoadUserByUsernameResult = this.customUserDetailService.loadUserByUsername("janedoe");
        assertEquals(1, actualLoadUserByUsernameResult.getAuthorities().size());
        assertEquals("janedoe", actualLoadUserByUsernameResult.getUsername());
        assertSame(user, ((UserPrincipal) actualLoadUserByUsernameResult).getUser());
        assertEquals("iloveyou", actualLoadUserByUsernameResult.getPassword());
        assertEquals(123L, ((UserPrincipal) actualLoadUserByUsernameResult).getId().longValue());
        verify(this.userService).findByUsername(any());
    }

    @Test
    void testLoadUserByUsername3() throws UsernameNotFoundException {
        when(this.userService.findByUsername(any())).thenReturn(Optional.empty());
        User user = mock(User.class);
        when(user.getId()).thenThrow(new UsernameNotFoundException("Msg"));
        when(user.getPassword()).thenThrow(new UsernameNotFoundException("Msg"));
        when(user.getRole()).thenReturn(Role.USER);
        doNothing().when(user).setBooks(any());
        doNothing().when(user).setCoupons(any());
        doNothing().when(user).setCreateTime(any());
        doNothing().when(user).setEmail(any());
        doNothing().when(user).setId(any());
        doNothing().when(user).setPassword(any());
        doNothing().when(user).setRole(any());
        doNothing().when(user).setToken(any());
        doNothing().when(user).setUsername(any());
        user.setBooks(new ArrayList<>());
        user.setCoupons(new ArrayList<>());
        user.setCreateTime(LocalDateTime.of(1, 1, 1, 1, 1));
        user.setEmail("jane.doe@example.org");
        user.setId(123L);
        user.setPassword("iloveyou");
        user.setRole(Role.USER);
        user.setToken("ABC123");
        user.setUsername("janedoe");
        assertThrows(UsernameNotFoundException.class, () -> this.customUserDetailService.loadUserByUsername("janedoe"));
        verify(this.userService).findByUsername(any());
        verify(user).setBooks(any());
        verify(user).setCoupons(any());
        verify(user).setCreateTime(any());
        verify(user).setEmail(any());
        verify(user).setId(any());
        verify(user).setPassword(any());
        verify(user).setRole(any());
        verify(user).setToken(any());
        verify(user).setUsername(any());
    }
}

