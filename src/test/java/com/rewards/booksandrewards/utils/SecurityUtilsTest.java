package com.rewards.booksandrewards.utils;

import com.rewards.booksandrewards.model.enums.Role;
import org.junit.jupiter.api.Test;

import javax.servlet.http.HttpServletRequest;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class SecurityUtilsTest {
    HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);

    @Test
    public void checkConvertToAuthority_WithRolePrefix() {
        String actual = SecurityUtils.convertToAuthority("ROLE_" + Role.ADMIN.name()).getAuthority();
        assertThat(actual).isEqualTo("ROLE_ADMIN");
    }

    @Test
    public void checkConvertToAuthority_WithoutRolePrefix() {
        String actual = SecurityUtils.convertToAuthority(Role.ADMIN.name()).getAuthority();
        assertThat(actual).isEqualTo("ROLE_ADMIN");
    }

    @Test
    public void extractAuthTokenFromRequest_ValidToken() {
        when(httpServletRequest.getHeader(anyString())).thenReturn("Bearer token");

        String token = SecurityUtils.extractAuthenticationTokenFromRequest(httpServletRequest);
        assertThat(token).isEqualTo("token");
    }


    @Test
    public void extractAuthTokenFromRequest_InvalidToken() {
        when(httpServletRequest.getHeader(anyString())).thenReturn("Bearers token");

        String token = SecurityUtils.extractAuthenticationTokenFromRequest(httpServletRequest);
        assertThat(token).isEqualTo(null);
    }

    @Test
    public void extractAuthTokenFromRequest_NoToken() {
        when(httpServletRequest.getHeader(anyString())).thenReturn("Bearer");

        String token = SecurityUtils.extractAuthenticationTokenFromRequest(httpServletRequest);
        assertThat(token).isEqualTo(null);
    }

    @Test
    public void extractAuthTokenFromRequest_EmptyString() {
        when(httpServletRequest.getHeader(anyString())).thenReturn("");

        String token = SecurityUtils.extractAuthenticationTokenFromRequest(httpServletRequest);
        assertThat(token).isEqualTo(null);
    }
}
