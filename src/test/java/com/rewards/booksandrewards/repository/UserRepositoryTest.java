package com.rewards.booksandrewards.repository;

import com.rewards.booksandrewards.model.User;
import com.rewards.booksandrewards.model.enums.Role;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.assertj.core.api.Assertions.*;

import org.springframework.boot.test.context.SpringBootTest;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.Optional;

@SpringBootTest
class UserRepositoryTest {

    @Autowired
    private UserRepository userRepository;

    @Test
    @Disabled
    @Transactional
    public void updateUserRole_ChangeRole() {
        User user = new User();
        user.setUsername("testing");
        user.setPassword("testing");
        user.setEmail("testing");
        user.setRole(Role.USER);
        user.setCreateTime(LocalDateTime.now());

        userRepository.save(user);

        try{
            userRepository.updateUserRole("testing", Role.ADMIN);
            Optional<User> updatedUser = userRepository.findByUsername("testing");
            if (!updatedUser.isPresent()) {
                fail("User not found");
                return;
            }
            assertThat(updatedUser.get().getRole()).isEqualTo(Role.ADMIN);
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }
}