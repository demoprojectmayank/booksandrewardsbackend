package com.rewards.booksandrewards.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.rewards.booksandrewards.model.Book;
import com.rewards.booksandrewards.service.BookService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {BookController.class})
class BookControllerTest {

    public static final MediaType APPLICATION_JSON_UTF8 = new MediaType(MediaType.APPLICATION_JSON.getType(), MediaType.APPLICATION_JSON.getSubtype(), StandardCharsets.UTF_8);

    @MockBean
    private BookService bookService;

    @Autowired
    private BookController bookController;

    private MockMvc mockMvc;

    private static ObjectWriter ow;

    @BeforeAll
    static void beforeAll() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ow = mapper.writer().withDefaultPrettyPrinter();
    }

    @BeforeEach
    void setUp() {
        mockMvc = MockMvcBuilders.standaloneSetup(bookController).build();
    }

    @Test
    public void getAllBooks() throws Exception {
        when(this.bookService.getAllBooks()).thenReturn(new ArrayList<>());

        MvcResult result = mockMvc.perform(get("/api/admin/books"))
                .andExpect(status().isOk())
                .andReturn();

        String content = result.getResponse().getContentAsString();
        assertThat(content).isEqualTo("[]");
    }

    @Test
    public void saveBook() throws Exception {
        Book book = new Book();
        book.setBookName("testing");
        book.setPrice(10L);
        book.setQuantity(20L);
        book.setAuthorName("testing");

        when(this.bookService.saveBook(any())).thenReturn(book);

        String requestJson = ow.writeValueAsString(book);

        mockMvc.perform(post("/api/admin/books")
                        .contentType(APPLICATION_JSON_UTF8)
                        .content(requestJson))
                .andExpect(status().isCreated());
    }

    @Test
    public void getBooksByUsername() throws Exception {
        when(bookService.getAllBooks()).thenReturn(new ArrayList<>());
        mockMvc.perform(get("/api/user/books/testingUser"))
                .andExpect(status().isOk());
    }

    @Test
    public void buyBook() throws Exception {
        MvcResult result = mockMvc.perform(get("/api/books/buy/testingUsername/24"))
                .andExpect(status().isOk())
                .andReturn();
        String response = result.getResponse().getContentAsString();
        assertThat(response).isEqualTo("Purchased");
    }

    @Test
    public void buyBook_Exception() throws Exception {
        doThrow(new Exception("Purchase Failed")).when(bookService).buyBook(anyString(), anyLong());
        MvcResult result = mockMvc.perform(get("/api/books/buy/testingUsername/24"))
                .andExpect(status().isNotFound())
                .andReturn();
        String response = result.getResponse().getContentAsString();
        assertThat(response).isEqualTo("Purchase Failed");
    }
}


