package com.rewards.booksandrewards.controller;

import static org.mockito.Mockito.any;
import static org.mockito.Mockito.when;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rewards.booksandrewards.model.User;
import com.rewards.booksandrewards.model.enums.Role;
import com.rewards.booksandrewards.service.AuthenticationService;
import com.rewards.booksandrewards.service.UserService;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@ContextConfiguration(classes = {AuthenticationController.class})
@ExtendWith(SpringExtension.class)
class AuthenticationControllerTest {
    @Autowired
    private AuthenticationController authenticationController;

    @MockBean
    private AuthenticationService authenticationService;

    @MockBean
    private UserService userService;

    @Test
    void testSignIn() throws Exception {
        User user = new User();
        user.setBooks(new ArrayList<>());
        user.setCoupons(new ArrayList<>());
        user.setCreateTime(LocalDateTime.of(1, 1, 1, 1, 1));
        user.setEmail("jane.doe@example.org");
        user.setId(123L);
        user.setPassword("iloveyou");
        user.setRole(Role.USER);
        user.setToken("ABC123");
        user.setUsername("janedoe");
        when(this.authenticationService.signInAndReturnJwt(any())).thenReturn(user);

        User user1 = new User();
        user1.setBooks(new ArrayList<>());
        user1.setCoupons(new ArrayList<>());
        user1.setCreateTime(null);
        user1.setEmail("jane.doe@example.org");
        user1.setId(123L);
        user1.setPassword("iloveyou");
        user1.setRole(Role.USER);
        user1.setToken("ABC123");
        user1.setUsername("janedoe");
        String content = (new ObjectMapper()).writeValueAsString(user1);
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post("/api/authentication/sign-in")
                .contentType(MediaType.APPLICATION_JSON)
                .content(content);
        MockMvcBuilders.standaloneSetup(this.authenticationController)
                .build()
                .perform(requestBuilder)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType("application/json"))
                .andExpect(MockMvcResultMatchers.content()
                        .string(
                                "{\"id\":123,\"username\":\"janedoe\",\"email\":\"jane.doe@example.org\",\"createTime\":[1,1,1,1,1],\"role\":\"USER\""
                                        + ",\"coupons\":[],\"books\":[],\"token\":\"ABC123\"}"));
    }

    @Test
    void testSignUp() throws Exception {
        User user = new User();
        user.setBooks(new ArrayList<>());
        user.setCoupons(new ArrayList<>());
        user.setCreateTime(LocalDateTime.of(1, 1, 1, 1, 1));
        user.setEmail("jane.doe@example.org");
        user.setId(123L);
        user.setPassword("iloveyou");
        user.setRole(Role.USER);
        user.setToken("ABC123");
        user.setUsername("janedoe");

        User user1 = new User();
        user1.setBooks(new ArrayList<>());
        user1.setCoupons(new ArrayList<>());
        user1.setCreateTime(LocalDateTime.of(1, 1, 1, 1, 1));
        user1.setEmail("jane.doe@example.org");
        user1.setId(123L);
        user1.setPassword("iloveyou");
        user1.setRole(Role.USER);
        user1.setToken("ABC123");
        user1.setUsername("janedoe");
        Optional<User> ofResult = Optional.of(user1);
        when(this.userService.createNewUser(any())).thenReturn(user);
        when(this.userService.findByUsername(any())).thenReturn(ofResult);

        User user2 = new User();
        user2.setBooks(new ArrayList<>());
        user2.setCoupons(new ArrayList<>());
        user2.setCreateTime(null);
        user2.setEmail("jane.doe@example.org");
        user2.setId(123L);
        user2.setPassword("iloveyou");
        user2.setRole(Role.USER);
        user2.setToken("ABC123");
        user2.setUsername("janedoe");
        String content = (new ObjectMapper()).writeValueAsString(user2);
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post("/api/authentication/sign-up")
                .contentType(MediaType.APPLICATION_JSON)
                .content(content);
        ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.authenticationController)
                .build()
                .perform(requestBuilder);
        actualPerformResult.andExpect(MockMvcResultMatchers.status().is(409));
    }

    @Test
    void testSignUp2() throws Exception {
        User user = new User();
        user.setBooks(new ArrayList<>());
        user.setCoupons(new ArrayList<>());
        user.setCreateTime(LocalDateTime.of(1, 1, 1, 1, 1));
        user.setEmail("jane.doe@example.org");
        user.setId(123L);
        user.setPassword("iloveyou");
        user.setRole(Role.USER);
        user.setToken("ABC123");
        user.setUsername("janedoe");
        when(this.userService.createNewUser(any())).thenReturn(user);
        when(this.userService.findByUsername(any())).thenReturn(Optional.empty());

        User user1 = new User();
        user1.setBooks(new ArrayList<>());
        user1.setCoupons(new ArrayList<>());
        user1.setCreateTime(null);
        user1.setEmail("jane.doe@example.org");
        user1.setId(123L);
        user1.setPassword("iloveyou");
        user1.setRole(Role.USER);
        user1.setToken("ABC123");
        user1.setUsername("janedoe");
        String content = (new ObjectMapper()).writeValueAsString(user1);
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post("/api/authentication/sign-up")
                .contentType(MediaType.APPLICATION_JSON)
                .content(content);
        ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.authenticationController)
                .build()
                .perform(requestBuilder);
        actualPerformResult.andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.content().contentType("application/json"))
                .andExpect(MockMvcResultMatchers.content()
                        .string(
                                "{\"id\":123,\"username\":\"janedoe\",\"email\":\"jane.doe@example.org\",\"createTime\":[1,1,1,1,1],\"role\":\"USER\""
                                        + ",\"coupons\":[],\"books\":[],\"token\":\"ABC123\"}"));
    }
}

