package com.rewards.booksandrewards.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.rewards.booksandrewards.model.Coupon;
import com.rewards.booksandrewards.service.CouponService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {CouponController.class})
class CouponControllerTest {

    @Autowired
    private CouponController couponController;

    @MockBean
    private CouponService couponService;

    private static ObjectWriter ow;

    private static Coupon coupon;

    private static ObjectMapper mapper;

    @BeforeAll
    static void beforeAll() {
        mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ow = mapper.writer().withDefaultPrettyPrinter();

        coupon = new Coupon();
        coupon.setCouponName("Testing Coupon");
        coupon.setCouponValue(20L);
    }

    MockMvc mockMvc;

    @BeforeEach
    void setUp() {
        mockMvc = MockMvcBuilders.standaloneSetup(couponController).build();
    }

    @Test
    public void getAllCoupons() throws Exception {
        when(couponService.getAllCoupons()).thenReturn(new ArrayList<>());
        String response = mockMvc.perform(get("/api/admin/coupons"))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();
        assertThat(response).isEqualTo("[]");
    }

    @Test
    public void addCoupon_returnTrue() throws Exception {
        String jsonBody = ow.writeValueAsString(coupon);

        when(couponService.addCoupon(any(), any())).thenReturn(true);
        mockMvc.perform(post("/api/admin/coupons/add/testingUsername")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonBody))
                .andExpect(status().isCreated());
    }

    @Test
    public void addCoupon_returnFalse() throws Exception {
        String jsonBody = ow.writeValueAsString(coupon);

        when(couponService.addCoupon(any(), any())).thenReturn(false);
        mockMvc.perform(post("/api/admin/coupons/add/testingUsername")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonBody))
                .andExpect(status().isNotAcceptable());
    }

    @Test
    public void getCouponsByUsername() throws Exception {
        when(couponService.getCouponsByUsername(any())).thenReturn(Collections.singletonList(coupon));
        ObjectWriter customObjectWriter = mapper.writer();
        String jsonBody = customObjectWriter.writeValueAsString(coupon);

        String expectedResponse = mockMvc.perform(get("/api/user/coupons/testingUsername"))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();

        assertThat(expectedResponse).isEqualTo("[" + jsonBody + "]");
    }
}