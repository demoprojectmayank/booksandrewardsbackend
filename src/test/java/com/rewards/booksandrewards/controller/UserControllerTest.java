package com.rewards.booksandrewards.controller;

import com.rewards.booksandrewards.model.enums.Role;
import com.rewards.booksandrewards.security.UserPrincipal;
import com.rewards.booksandrewards.service.CouponService;
import com.rewards.booksandrewards.service.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MockMvcBuilder;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.ContentResultMatchers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {UserController.class})
class UserControllerTest {

    @MockBean
    private UserService userService;

    @MockBean
    private CouponService couponService;

    @Autowired
    private UserController userController;

    private MockMvc mockMvc;

    @BeforeEach
    void setUp() {
        mockMvc = MockMvcBuilders.standaloneSetup(userController).build();
    }

    @Test
    void testChangeRole() throws Exception {
        doNothing().when(this.userService).changeRole(any(), any());
        mockMvc.perform(put("/api/users/change/{role}", Role.USER))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))
                .andExpect(content().string("true"));
    }

    @Test
    public void getAllUsers() throws Exception {
        when(userService.getAllUser()).thenReturn(new ArrayList<>());
        mockMvc.perform(get("/api/users"))
                .andExpect(status().isOk())
                .andExpect(content().string("[]"));
    }

    @Test
    void testChangeRole_switchRole() throws Exception {
        when(userService.switchRole(any())).thenReturn(true);
        mockMvc.perform(put("/api/users/switch-role/{username}", "janedoe"))
                .andExpect(status().isOk())
                .andExpect(content().string("true"))
                .andExpect(content().contentType("application/json"));
    }

    @Test
    void testRewardTicket() throws Exception {
        when(this.userService.rewardTicketToUser(any(), any(), any())).thenReturn(true);
        when(this.couponService.getCouponsByUsername(any())).thenReturn(new ArrayList<>());
        mockMvc.perform(put("/api/users/rewards/{couponId}/{username}",
                        123L, "janedoe"))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))
                .andExpect(content().string("[]"));
    }

    @Test
    void testRewardTicket2() throws Exception {
        when(this.userService.rewardTicketToUser(any(), any(), any())).thenReturn(false);
        when(this.couponService.getCouponsByUsername(any())).thenReturn(new ArrayList<>());
        mockMvc.perform(put("/api/users/rewards/{couponId}/{username}",
                        123L, "janedoe"))
                .andExpect(status().is(400));
    }

    @Test
    void testRewardTicket3() throws Exception {
        when(this.userService.rewardTicketToUser(any(), any(), any())).thenReturn(true);
        when(this.couponService.getCouponsByUsername(any())).thenReturn(new ArrayList<>());
        mockMvc.perform(put("/api/users/rewards/{couponId}/{username}",
                        123L, "janedoe"))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))
                .andExpect(content().string("[]"));
    }
}